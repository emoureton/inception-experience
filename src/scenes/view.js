import { HemisphereLight, SpotLight, Vector3 } from 'three';

import { Base } from './base'
import { Skybox } from './global/skybox';
import { Totems } from './phase/totems';
import { Loader } from './global/loader';

export class View extends Base {
  constructor(app) {
    super()
    this.app = app
  }

  start({ container, dreaming }) {
    this.init(container)
    this.dreaming = dreaming
    this.loader()
    this.ended = new Event('ended');
  }

  loader() {
    this.addLights()
    this.totems = new Totems(this.scene, this.dreaming)
    this.skybox = new Skybox(this.scene, () => {
      this.explanations()
    })
    this.loader = new Loader()
    this.update()
  }

  explanations() {
    // Callback fired by end of dreams
    document.dispatchEvent(this.ended);
    this.totems.moveRight()
  }

  addLights() {
    // Create hemisphere light
    const hemisphereLight = new HemisphereLight(0xffffff, 0x5e5d63, 4);
    this.scene.add(hemisphereLight);

    // Create spot light
    const spotLight = new SpotLight(0xffffff, 10, 100, Math.PI / 5);
    spotLight.target.position.set(new Vector3(0, 0, 0));
    spotLight.castShadow = true;
    spotLight.position.set(10, 50, 2);
    spotLight.name = "spot"
    this.scene.add(spotLight);

    const spotLight2 = new SpotLight(0xffffff, 1, 100, Math.PI / 5);
    spotLight2.target.position.set(new Vector3(0, 0, 0));
    spotLight2.castShadow = true;
    spotLight2.position.set(-50, -50, 10);
    spotLight2.name = "spot2"
    this.scene.add(spotLight2);
  }

  update() {
    window.requestAnimationFrame(() => {
      this.update()
    })
  }
}

// ----------- Helpers ----------------- //

const randomNumber = (min, max) => { return Math.random() * (max - min) + min }