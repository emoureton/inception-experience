import { CanvasTexture, MeshPhysicalMaterial, RepeatWrapping, Vector2 } from 'three';
import { FlakesTexture } from "three/examples/jsm/textures/FlakesTexture"
import gsap from "gsap";

const maxLeanAngle = 0.75943
const speedYRotation = 1
const speedZRotation = (dreaming) => {
 return dreaming === true ? 0 : 0.005
}

export class SpinningTop {
  constructor(scene) {
    this.scene = scene
    this.init()
  }

  init() {
    this.spinningTop = this.scene.getObjectByName('cobb_totem')
    this.spinningTop.position.set(0, 100, 0)
    this.spinningTop.scale.set(0.4, 0.4, 0.4)

    this.spinningTop.traverse((node) => {
      let texture = new CanvasTexture(new FlakesTexture())
      texture.wrapS = RepeatWrapping
      texture.wrapT = RepeatWrapping
      texture.repeat.x = 10
      texture.repeat.y = 6

      const spinMaterial = {
        clearcoat: 1.0,
        clearcoatRoughness: 0.1,
        metalness: 0.9,
        roughness: 0.5,
        color: 0x4d4d4d,
        normalMap: texture,
        normalScale: new Vector2(0.15, 0.15)
      }

      const mat = new MeshPhysicalMaterial(spinMaterial);
      if (node.isMesh) node.material = mat
    });

    gsap.to(this.spinningTop.position, {
      y: 0,
      duration: 2,
      ease: "power2.out"
    })

    return this.spinningTop
  }

  launch() {
    const tl = gsap.timeline()
    tl.to(this.spinningTop.rotation, {
      z: 0.0174533,
      duration: 0.2,
      ease: "power1.out"
    })
    tl.to(this.spinningTop.scale, {
      y: 0.6,
      x: 0.6,
      z: 0.6,
      duration: 3,
      delay: 1,
      ease: "power1.out"
    })
    this.animateSpin()
  }

  animateSpin() {
    const spin = this.scene.getObjectByName('cobb_totem-container')
    if(spin.rotation.z < maxLeanAngle) {
      gsap.to(spin.rotation, {
        y: spin.rotation.y + speedYRotation,
        z: spin.rotation.z + speedZRotation(this.dreaming)
      })

      window.requestAnimationFrame(() => {
        this.animateSpin()
      })
    }
  }
}