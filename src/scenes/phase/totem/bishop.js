import { CanvasTexture, MeshPhysicalMaterial, RepeatWrapping, Vector2 } from "three"
import { FlakesTexture } from "three/examples/jsm/textures/FlakesTexture"

export class Bishop {
  constructor(scene, obj) {
    this.scene = scene
    this.init()
  }

  init() {
    this.bishop = this.scene.getObjectByName('ariane_totem')
    this.bishopContainer = this.scene.getObjectByName('ariane_totem-container')
    this.bishopContainer.scale.set(0, 0, 0)
    this.bishop.scale.set(0.5, 0.5, 0.5)
    this.bishop.position.y = -6
    this.bishop.traverse((node) => {
      let texture = new CanvasTexture(new FlakesTexture())
      texture.wrapS = RepeatWrapping
      texture.wrapT = RepeatWrapping
      texture.repeat.x = 10
      texture.repeat.y = 6

      const spinMaterial = {
        clearcoat: 1.0,
        clearcoatRoughness: 0.1,
        metalness: 0.9,
        roughness: 0.5,
        color: 0xCD853F,
        normalMap: texture,
        normalScale: new Vector2(0.15, 0.15)
      }

      const mat = new MeshPhysicalMaterial(spinMaterial);
      if (node.isMesh) node.material = mat
    });

    return this.bishop
  }
}