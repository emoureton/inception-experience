import { CanvasTexture, Group, MeshPhysicalMaterial, RepeatWrapping, Vector2 } from 'three';
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";
import { FlakesTexture } from "three/examples/jsm/textures/FlakesTexture"
import gsap from "gsap";
import { Bishop } from './totem/bishop';
import { SpinningTop } from './totem/spinning-top';

export class Totems {
  constructor(scene, dreaming) {
    this.scene = scene
    this.dreaming = dreaming
    this.loadedTotems = []
    this.totems = [
      {
        'person' : 'cobb',
        'asset' : '/assets/models/spinning-top/spinning-top.gltf'
      },
      {
        'person' : 'ariane',
        'asset' : '/assets/models/bishop/bishop.gltf'
      }
    ]
    this.loadModel(0)
  }

  loadModel(i) {
    const item = this.totems[i]
    const loader = new GLTFLoader();
    loader.load(item.asset, (gltf) => {
      const obj = gltf.scene
      obj.name = item.person + '_totem'
      this.loadedTotems.push(obj)
      if(this.loadedTotems.length < this.totems.length) {
        this.loadModel(i + 1)
      } else {
        this.create()
      }
    })
  }

  create() {
    this.totemsContainer = new Group()
    this.loadedTotems.forEach(totem => {
      const container = new Group()
      container.name = totem.name + '-container'
      container.add(totem)
      this.totemsContainer.add(container)
    })
    this.scene.add(this.totemsContainer)

    this.bishop = new Bishop(this.scene)
    this.spinningTop = new SpinningTop(this.scene)
    this.update()
  }

  switchTotem(person) {
    this.totems.forEach(infos => {
      const totem = this.scene.getObjectByName(infos.person + '_totem-container')
      if(infos.person === person) {
        gsap.to(totem.scale, {
          x: 1,
          y: 1,
          z: 1,
          duration: 0.3,
          ease: "power4.out"
        })
      } else {
        gsap.to(totem.scale, {
          x: 0,
          y: 0,
          z: 0,
          duration: 0.3,
          ease: "power4.out"
        })
      }
    })
  }

  moveRight() {
    gsap.to(this.totemsContainer.position, {
      x: 12,
      duration: 2,
      ease: "power1.inOut"
    })
  }

  update() {
    this.totemsContainer.rotation.y += 0.005

    window.requestAnimationFrame(() => {
      this.update()
    })
  }
}