import { BackSide, BoxBufferGeometry, BoxGeometry, Clock, Mesh, MeshBasicMaterial, ShaderMaterial, TextureLoader, Vector2 } from 'three';
import vertexShader from '../../../public/assets/shaders/fade/vertex.glsl'
import fragmentShader from '../../../public/assets/shaders/fade/fragment.glsl'
import gsap from 'gsap'

const sides = ["ft", "bk", "up", "dn", "rt", "lf"];

export class Skybox {
  constructor(scene, explanations) {
    this.scene = scene
    this.inTransition = false
    this.explanations = explanations
    this.create()
    this.update()
  }

  create() {
    this.clock = new Clock();

    const skyBoxWidth= 90
    const skyBoxHeight= 90
    const materialArray = this.createMaterialArray('clouds/');

    const geo = new BoxGeometry(skyBoxWidth, skyBoxHeight, skyBoxWidth);
    this.skybox = new Mesh(geo, materialArray);
    this.skybox.position.y = -10
    this.skybox.name = "skybox";
    this.skybox.receiveShadow = true
    this.skybox.castShadow = true
    this.scene.add(this.skybox)

    this.initTransition()

    return this.skybox
  }

  initTransition() {
    const dreams = [
      '/assets/img/dream/dream-1',
      '/assets/img/dream/dream-2',
      '/assets/img/clouds',
    ]
    this.tls = []
    this.skybox.material.forEach((el) => {
      const tl = gsap.timeline({
        delay: 4,
        paused: true
      })
      dreams.forEach((item, index) => {
        if(index <= dreams.length) {          
          if (el.name == 'up' || el.name == 'dn') {
            return;
          }

          const currentImg = new TextureLoader().load(item + '/' + el.name + '.jpg')
          this.prerenderTextureUglyWay(currentImg);

          let newImg;
          if (index < dreams.length-1) {
            newImg = new TextureLoader().load(dreams[index + 1] + '/' + el.name + '.jpg')
            this.prerenderTextureUglyWay(newImg);
          }


          tl.delay(1)
          tl.to(el.uniforms.u_circleradius, {
            value: 2,
            duration: 3.5,
            ease: 'power2.in',
          })
          
          tl.call(() => {

            el.uniforms.u_image.value = currentImg
            el.uniforms.u_circleradius.value = 0
            if (index < dreams.length) {
              el.uniforms.u_imagehover.value = newImg
            }
          })
        }
      })
      this.tls.push(tl)
    })

    this.tls[0].then(() => {
      this.explanations()
    })
  }

  transition() {
    this.tls.forEach((tl) => {
      tl.play()
    })
  }

  prerenderTextureUglyWay(texture) {
    const cube = new Mesh(
      new BoxBufferGeometry(),
      new MeshBasicMaterial({map: texture, transparent: true, opacity: 0})
    );
    cube.position.z = 10;
    this.scene.add(cube);
    // imgLoaded += 1
    // console.log(imgLoaded + '/' + imgToLoad)
  }

  update() {
    if(this.inTransition === false) {
      this.skybox.rotation.y += 0.001;
    }

    this.skybox.material.forEach((el) => {
      el.uniforms.u_time.value += 0.01;
    })

    window.requestAnimationFrame(() => {
      this.update()
    })
  }

  createPathStrings(filename) {
    const basePath = "/assets/img/";
    const baseFilename = basePath + filename;
    const fileType = ".jpg";
    const pathStings = sides.map(side => {
      return baseFilename + side + fileType;
    });
    return pathStings;
  }

  createMaterialArray(filename) {
    const skyboxImagepaths = this.createPathStrings(filename);
    const materialArray = skyboxImagepaths.map((image, index) => {
      const hoverImg = new TextureLoader().load('/assets/img/dream/dream-1/' + sides[index] + '.jpg')
      const img = new TextureLoader().load(image)
      const material =  new ShaderMaterial({
        vertexShader,
        fragmentShader,
        uniforms: {
          u_image: { type: 't', value: img },
          u_imagehover: { type: 't', value: hoverImg },
          u_pos: { value: new Vector2(0, 0) },
          u_time: { value: 0 },
          u_res: {
              value: new Vector2(window.innerWidth, window.innerHeight)
          },
          u_circleradius: { value: 0.0 },
          uTexture: { value: img },
        },
        side: BackSide,
        defines: {
          PR: window.devicePixelRatio.toFixed(1)
        }
      });
      material.name = sides[index]
      return material
    });
    return materialArray;
  }
}
