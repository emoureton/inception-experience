import { DefaultLoadingManager } from "three";

export class Loader {
  constructor() {
    this.init()
  }
  
  init() {
    DefaultLoadingManager.onLoad = () => {
      console.log( 'Loading Complete!');
    };

    DefaultLoadingManager.onProgress = ( url, itemsLoaded, itemsTotal ) => {
      // console.log(((itemsLoaded / itemsTotal) * 100) + '%')
      const percentage = (itemsLoaded / itemsTotal) * 100
      const event = new CustomEvent('assetsLoader', { detail: { percentage: percentage } })
      document.dispatchEvent(event);
    };
  } 
}