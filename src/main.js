import { createApp } from 'vue'
import App from './App.vue'
import ThreePlugin from './plugins/three'
import GSAP from './plugins/gsap'

const app = createApp(App)
app.use(ThreePlugin)
app.use(GSAP)
app.mount('#app')