import { View } from '../scenes/view'

const ThreePlugin = {
  install(app, options) {
    app.config.globalProperties.$three =  new View(app)
  }
}

export default ThreePlugin