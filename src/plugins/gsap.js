import gsap from "gsap";
import { SplitText } from "gsap/SplitText";
gsap.registerPlugin(SplitText)

const GSAP = {
  install(app) {
    app.config.globalProperties.$gsap = gsap
    app.config.globalProperties.$gsap.SplitText = SplitText
  }
}

export default GSAP